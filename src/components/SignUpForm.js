import React, { Component } from "react";
import validator from "validator";

class SignUpForm extends Component {
    constructor(props){
        super(props);

        this.state = {
            firstName: "",
            lastName: "",
            age: "",
            gender: "",
            role: "",
            email: "",
            password: "",
            repeatPassword: "",
            agreeTerms: 'false',
            errors: {
                firstNameError: '',
                lastNameError: '',
                ageError: '',
                genderError : '',
                roleError : '',
                emailError: '',
                passwordError : '',
                repeatPasswordError: '',
                agreeTermsError : '',
            },
            success: ''
          };
    }

    handleChange = (event) => {
        const { id, value,type } = event.target;
        if (type === 'checkbox'){
            if (this.state.agreeTerms === 'checked'){
                this.setState({
                    agreeTerms : 'false'
                })
            }
            else{
                this.setState({
                    agreeTerms : 'checked'
                })
            }
        }
        else{
            this.setState({ 
                [id]: value 
            });
        }
    };

    handleGenderChange = (event) => { 
        this.setState({
            gender: event.target.id
        });
    };

    handleRoleChange = (event) => {
        this.setState({ 
            role: event.target.value 
        });
    };


    handleSubmit = (event) =>{
        event.preventDefault();

        const {firstName,lastName,age,gender,role,email,password,repeatPassword,agreeTerms} = this.state;
        // const {firstNameError,lastNameError,ageError,genderError,roleError,emailError,passwordError,repeatPasswordError,agreeTermsError} = this.state.errors;

        let errorsList = {...this.state.errors};

        if(validator.isEmpty(firstName)){
            errorsList.firstNameError = "First Name cannot be empty."
        }
        else if(!validator.isAlpha(firstName)){
            errorsList.firstNameError = "First Name should only contain aplhabets."
        }
        else{
            errorsList.firstNameError = ""
        }

        if(validator.isEmpty(lastName)){
            errorsList.lastNameError = "Last Name cannot be empty."
        }
        else if(!validator.isAlpha(lastName)){
            errorsList.lastNameError = "Last Name should only contain aplhabets."
        }
        else{
            errorsList.lastNameError = ""
        }

        if(validator.isEmpty(age)){
            errorsList.ageError = "Age cannot be empty."
        }
        else if(!validator.isNumeric(age)){
            errorsList.ageError = "Age should only contain numeric value."
        }
        else if(age < 18){
            errorsList.ageError = "Minimum age is 18 or above"
        }
        else{
            errorsList.ageError = ""
        }

        if(validator.isEmpty(gender)){
            errorsList.genderError = "Select your gender."
        }
        else{
            errorsList.genderError = ""
        }

        if(validator.isEmpty(role) || role === 'Select your Role'){
            errorsList.roleError = "Select your role"
        }
        else{
            errorsList.roleError = ""
        }

        if(!validator.isEmail(email)){
            errorsList.emailError = "Provide a valid email"
        }
        else{
            errorsList.emailError = ""
        }

        if(password.length < 6 ){
            errorsList.passwordError = "Password must contain minimum 6 characters"
        }
        else if(password.length > 15){
            errorsList.passwordError = "Password can have maximum 15 characters"
        }
        else if(!validator.isAlphanumeric(password)){
            errorsList.passwordError = "Password should contain only number & letters"
        }
        else{
            errorsList.passwordError = ""
        }

        if(repeatPassword !==  password){
            errorsList.repeatPasswordError = "Enter same password as above"
        }
        else{
            errorsList.repeatPasswordError = ""
        }

        if(agreeTerms === 'false'){
            
            errorsList.agreeTermsError = "Check the terms and conditions to preceed."
        }
        else{
            errorsList.agreeTermsError = ""
        }

        this.setState({
            errors: errorsList
        })

        const validInputs = Object.values(errorsList).filter((errorString)=>{
            return (errorString.length > 0);
        })

        if(validInputs.length === 0){
            this.setState({
                success : "Sign Up successful."
            })
        }
        else{
            this.setState({
                success : ""
            })
        }
    }

    render() {

        const {firstNameError,lastNameError,ageError,genderError,roleError,emailError,passwordError,repeatPasswordError,agreeTermsError} = this.state.errors;
        return (
        <div className="form">
            <div className="container d-flex justify-content-center p-1 bg-dark border text-light rounded-4">


                <div className="col-lg-24 form-section p-3  d-flex flex-column p-3 align-items-center ">
                    
                    <div className="form-row mb-1">
                        <h2>Sign Up to Get Started</h2>
                    </div>

                    <form  onSubmit={this.handleSubmit}> 
                    <div className="form-row mb-2">
                        <i className="fa fa-user" aria-hidden="true"></i> 
                        <label className="mx-2" htmlFor="firstName">First Name</label>
                        <div>
                            <input  
                            id="firstName" 
                            type="text" 
                            onChange={this.handleChange}/>
                            {firstNameError && <div className="text-danger">{firstNameError}</div>} 
                        </div>
                    </div>

                    <div className="form-row mb-2">
                        <i className="fa fa-user" aria-hidden="true"></i> 
                        <label className="mx-2" htmlFor="lastName">Last Name</label>
                        <div>
                            <input id="lastName" 
                            type="text" 
                            onChange={this.handleChange}/>
                            {lastNameError && <div className="text-danger">{lastNameError}</div>} 
                        </div>
                    </div>

                    <div className="form-row mb-2">
                        <i className="fas fa-birthday-cake"></i>
                        <label className="mx-2" htmlFor="age">Age</label>
                        <div>
                        <input id="age" 
                        type="text"
                        onChange={this.handleChange} />
                        {ageError && <div className="text-danger">{ageError}</div>} 
                        </div>
                    </div>

                    <div className="form-row mb-2">
                        <i className="fa fa-venus-mars"></i>
                        <label className="mx-2" htmlFor="gender">Gender</label>
                        <div >
                            <input
                            className="gender-btn gender-btn1 "
                            type="radio"
                            name="gender"
                            id="male"
                            onChange={this.handleGenderChange}
                            />
                            <label htmlFor="male" className="radio-inline mx-1">
                                Male
                            </label>

                            <input
                            className="gender-btn mx-1"
                            type="radio"
                            name="gender"
                            id="female"
                            onChange={this.handleGenderChange}
                            />
                            <label htmlFor="female" className="radio-inline mx-1">
                                Female
                            </label>

                            <input
                            className="gender-btn mx-1"
                            type="radio"
                            name="gender"
                            id="others"
                            onChange={this.handleGenderChange}
                            />
                            <label htmlFor="others" className="radio-inline mx-1">
                                Others
                            </label>
                            {genderError && <div className="text-danger">{genderError}</div>} 
                        </div>
                    </div>

                    <div className="form-row mb-2">
                        <i className="fas fa-users"></i>
                        <label className="mx-2" htmlFor="role">Role</label>
                        <div>
                        <select id="role"  onChange={this.handleRoleChange} >
                            <option >Select your Role</option>
                            <option >Developer</option>
                            <option >Senior Developer</option>
                            <option >Lead Engineer</option>
                            <option >CTO</option>
                        </select>
                        {roleError && <div className="text-danger">{roleError}</div>} 
                        </div>
                    </div>

                    <div className="form-row mb-2">
                        <i className="fa fa-envelope" aria-hidden="true"></i>
                        <label className="mx-2" htmlFor="email">Email</label>
                        <div>
                        <input id="email" 
                        type="email"
                        onChange={this.handleChange}/>      
                        {emailError && <div className="text-danger">{emailError}</div>} 
                        </div>
                    </div>

                    <div className="form-row mb-2">                        
                        <i className="fa fa-key" aria-hidden="true"></i>
                        <label className="mx-2" htmlFor="password">Password</label>
                        <div>
                        <input id="password" 
                        type="password"  
                        onChange={this.handleChange}/>
                        {passwordError && <div className="text-danger">{passwordError}</div>} 
                        </div>
                    </div>

                    <div className="form-row mb-2">
                        <i className="fa fa-key" aria-hidden="true"></i>
                        <label className="mx-2" htmlFor="repeatPassword">Repeat Password</label>
                        <div>
                        <input id="repeatPassword" 
                        type="password" 
                        onChange={this.handleChange}/>
                        {repeatPasswordError && <div className="text-danger">{repeatPasswordError}</div>}                         
                        </div>
                    </div>

                    <div className="form-row mb-2">
                        <input id="agreeTerms" 
                        type="checkbox" 
                        value={this.state.agreeTerms}
                        onChange = {this.handleChange}
                        />
                        <label htmlFor="agreeTerms ">
                            <div
                            className="text-decoration-none text-light mx-2 " 
                            >I Agree to terms and conditions</div>
                        </label>
                        {agreeTermsError && <div className="text-danger">{agreeTermsError}</div>}                         
                    </div>

                    <div className="form-row ">
                        <button className="btn btn-primary w-100" type="submit">Sign Up</button>
                        {this.state.success && <div className="text-success">{this.state.success}</div>}                         
                    </div>

                    </form>
                </div>
            </div>
        </div>
        );
    }
}


export default SignUpForm;
